<?php require_once "system/sessionHandler.php"; ?>
<!DOCTYPE html>
<html>
<head>
    <title>Star Trek Compation</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php require_once "template/assets.php"; ?>
</head>
<body>
    <div data-role="page">

        <div data-role="header" data-theme="b">
            <?php require_once "template/header.php"; ?>
        </div>
        <div data-role="content" data-theme="b">


<!-- FACEBOOK BOILERPLATE BEGIN -->
    <div id="fb-root"></div>
    <script>
        // Additional JS functions here
        window.fbAsyncInit = function() {
        FB.init({
        appId      : '506374912751734', // App ID
        channelUrl : '//www.ncc1705.com/MemoryGamma/channel.htm', // Channel File
        status     : true, // check login status
        cookie     : true, // enable cookies to allow the server to access the session
        xfbml      : true  // parse XFBML
        });

        // Additional init code here




  FB.Event.subscribe('auth.authResponseChange', function(response) {
    // Here we specify what we do with the response anytime this event occurs. 
    if (response.status === 'connected') {
      // The response object is returned with a status field that lets the app know the current
      // login status of the person. In this case, we're handling the situation where they 
      // have logged in to the app.
      testAPI();
    } else if (response.status === 'not_authorized') {
      // In this case, the person is logged into Facebook, but not into the app, so we call
      // FB.login() to prompt them to do so. 
      // In real-life usage, you wouldn't want to immediately prompt someone to login 
      // like this, for two reasons:
      // (1) JavaScript created popup windows are blocked by most browsers unless they 
      // result from direct interaction from people using the app (such as a mouse click)
      // (2) it is a bad experience to be continually prompted to login upon page load.
      FB.login();
    } else {
      // In this case, the person is not logged into Facebook, so we call the login() 
      // function to prompt them to do so. Note that at this stage there is no indication
      // of whether they are logged into the app. If they aren't then they'll see the Login
      // dialog right after they log in to Facebook. 
      // The same caveats as above apply to the FB.login() call here.
      FB.login();
    }
  });


    };

    // Load the SDK asynchronously
    (function(d){
    var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement('script'); js.id = id; js.async = true;
    js.src = "//connect.facebook.net/en_US/all.js";
    ref.parentNode.insertBefore(js, ref);
    }(document));


      // Here we run a very simple test of the Graph API after login is successful. 
  // This testAPI() function is only called in those cases. 
  function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me', function(response) {
      console.log('Good to see you, ' + response.name + '.');
    });
  }

    </script>

    <!--  Below we include the Login Button social plugin. This button uses the JavaScript SDK to
    present a graphical Login button that triggers the FB.login() function when clicked.

    Learn more about options for the login button plugin:
    /docs/reference/plugins/login/ -->

    <fb:login-button show-faces="true" width="200" max-rows="1"></fb:login-button><b>Register with Facebook(feature still in development)</b>
<!-- FACEBOOK BOILERPLATE END-->



            <a href="Star-Trek/" data-role="button">Episode Catalog</a>
            <a href="user/logIn.php" data-role="button" <?php if ($isLoggedIn){echo' class="ui-disabled"';}?>>Log In</a>
            <a href="user/register.php" data-role="button">Register (to save your viewing process)</a>
        </div>
        <div data-role="footer" id="test" data-theme="b">
            <?php include_once "template/footer.php"; ?>
        </div>
    </div>
</body>
</html>