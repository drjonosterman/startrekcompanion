<?php require_once "../system/sessionHandler.php";?>
<!DOCTYPE html>
<html>
<head>
<title>Star Trek Viewer's Log</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
  <div data-role="page">
    <?php echo '<script type="text/javascript" src="http://'.$host.'/MemoryGamma/lib/jsLoadCheck.js"></script>'; ?>
    <div data-role="header" data-theme="b">
        <?php require_once "../template/header.php"; ?>
    </div>
    <div data-role="content" data-theme="b" id="franchiseView">
      <a href="The-Original-Series/" data-role="button">The Original Series</a>
      <a href="The-Animated-Series/" data-role="button" id="TAS">The Animated Series</a>
      <a href="The-Next-Generation/" data-role="button" id="TNG">The Next Generation</a>
      <a href="Voyager/" data-role="button" id="VOY">Voyager</a>
      <a href="Deep-Space-Nine/" data-role="button" id="DS9">Deep Space Nine</a>
      <a href="Enterprise/" data-role="button" id="ENT">Enterprise</a>
      <a href="Movies/" data-role="button" id="MOV">Films</a>
    </div>
    <div data-role="footer" id="test" data-theme="b">
            <?php include_once "../template/footer.php"; ?>
    </div>
  </div>
</body>
</html>