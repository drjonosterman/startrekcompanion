<?php require_once "../system/sessionHandler.php"; ?>
<!DOCTYPE html>
<html>
<head>
    <title>Star Trek Companion</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <div data-role="page" id="page-FilmLevel">
        <?php echo '<script type="text/javascript" src="http://'.$host.'/MemoryGamma/lib/jsLoadCheck.js"></script>'; ?>
        <div data-role="header" data-theme="b">
            <?php require_once "../template/header.php"; ?>
        </div>
        <div data-role="content" id="filmLevelContent" data-theme="b">
            <h1 id="film-title">FILM</h1>
            <hr />
            <a href="#" data-role="button" id="navPrev"data-icon="arrow-l" data-inline="true">Previous</a>
            <a href="#" data-role="button" id="setBookmark" data-icon="check" data-inline="true" <?php if (!$isLoggedIn){echo' class="ui-disabled"';}?>>Save As Current Location</a>
            <a href="#" data-role="button" id="navNext" data-icon="arrow-r" data-inline="true">Next</a>
            <a href="#" data-role="button" id="saveFavorite" data-icon="star" <?php if (!$isLoggedIn){echo' class="ui-disabled"';}?>>favorite</a>
            <div data-role="collapsible" <?php if (!$isLoggedIn){echo' class="ui-disabled"';}?>>
                <h3>Leave a note</h3>
                <textarea id="note"></textarea>
                <a href="#" data-role="button" id="saveNote" data-inline="true">save note</a>
            </div>
            <div data-role="collapsible" <?php if (!$isLoggedIn){echo' class="ui-disabled"';}?>>
                <h3>Rate it!</h3>
                <input id="rating" type="range" min="0" max="10" step=".5" />
                <a href="#" data-role="button" id="sendRating" data-inline="true">submit</a>
            </div>
        </div>
        <div data-role="footer" id="test" data-theme="b">
            <?php include_once "../template/footer.php"; ?>
        </div>
    </div>
</body>
</html>