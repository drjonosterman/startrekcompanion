<?php require_once "../system/sessionHandler.php"; ?>
<!DOCTYPE html>
<html>
<head>
    <title>Star Trek Compation</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <div data-role="page" id="page-SeasonLevel">
        <?php echo '<script type="text/javascript" src="http://'.$host.'/MemoryGamma/lib/jsLoadCheck.js"></script>'; ?>
        <div data-role="header" data-theme="b">
        <?php require_once "../template/header.php"; ?>
        </div>
        <div data-role="content" id="seasonLevelContent" data-theme="b">
        </div>
        <div data-role="footer" id="test" data-theme="b">
            <?php include_once "../template/footer.php"; ?>
        </div>
    </div>
</body>
</html>