$('#page-SeriesLevel').live('pageshow', showLevelScript);
$('#page-SeasonLevel').live('pageshow', seasonLevelScript);
$('#page-EpisodeLevel').live('pageshow', episodeLevelScript);
$('#page-FilmLevel').live('pageshow', movieLevelScript);
$('#saveFavorite').live('tap', function(){ajaxCall('fave');});
$('#sendRating').live('tap', function(){ajaxCall('rating');});
$('#setBookmark').live('tap', function(){ajaxCall('bookmrk');});
$('#saveNote').live('tap', function(){ajaxCall('note');});
$('#navPrev').live('tap', prevEp);
$('#navNext').live('tap', nextEp);
$('#page-Register').live('pageshow', registerScript);

var app = { 
  showTitles: ['The-Original-Series', 'The-Animated-Series', 'The-Next-Generation', 'Deep-Space-Nine', 'Voyager', 'Enterprise', 'Movies'],
  acronyms: ['TOS', 'TAS', 'TNG', 'DS9', 'VOY', 'ENT', 'MOV'],
  shows: {'The-Original-Series':3, 'The-Animated-Series':2, 'The-Next-Generation':7, 'Deep-Space-Nine':7, 'Voyager':7, 'Enterprise':4},
  movies :["Star Trek: The Motion Picture", "Star Trek II: The Wrath of Khan", "Star Trek III: The Search for Spock", "Star Trek IV: The Voyage Home", "Star Trek V: The Final Frontier", "Star Trek VI: The Undiscovered Country", "Star Trek Generations", "Star Trek: First Contact", "Star Trek: Insurrection", "Star Trek: Nemesis", "Star Trek", "Star Trek into Darkness"],
  getHost: function() {return document.URL.indexOf('localhost') > 0 ? 'localhost' : 'ncc1705.com'},
  getShow: function() {return document.URL.match(/Star-Trek\/[\w-]{1,}/)[0].substr(10)},
  getSeason: function() {return document.URL.match(/\/[0-9]{1,2}\//)[0].replace(/\//g, '');},
  getEpisode: function() {return document.URL.match(/\/\d{1,2}/g)[1].replace('/', '')},
  getEpAsInt: function() {return parseInt(this.getEpisode());},
  getEpKey: function() {return this.acronyms[this.showTitles.indexOf(this.getShow())] + '-' + this.getSeason() + '-' + this.getEpisode();},
  seasonTitles: null};

var ajaxData = {
  successCallback: function(data){console.log(data)},
  rating: function(){return {type:'getRating', epId:app.getEpKey(), rating:$('#rating').val()}}, 
  bookmrk: function(){return {type:'setBookmark', epId:app.getEpKey()}},
  note: function(){return {type:'saveNote', epId:app.getEpKey(), note:$('#note').val()}},
  fave: function(){return {type:'saveFavorite', epId:app.getEpKey()}}
};

  function showLevelScript(){ 
    var currShow = app.getShow();
    var displayStr = '';

    if (currShow === 'Movies')
    {
      for (var i = 0 ; i < app.movies.length ; i++) 
      {
        displayStr += "<a href=\"" + (i+1) + "/\" data-role=\"button\">" + app.movies[i] + "</a>";
      }
    }
    else
    {
      for (var i = 0 ; i < app.shows[currShow] ; i++)
      {
        displayStr += "<a href=\"" + (i+1) + "/\" data-role=\"button\">Season " + (i+1) + "</a>";
      }
    }
    $('#seriesLevelContent').append(displayStr).trigger('create');
  }

  function seasonLevelScript(){
    $.ajax({
    type: "POST",
    url: "http://" + app.getHost() + "/MemoryGamma/system/ajaxHandler.php",
    data: {type:'returnSeason', showId:app.getShow(), seasonId:app.getSeason()},
    success: seasonCallback,
    dataType:'json'});

    function seasonCallback(season){
      app.seasonTitles = season;
      var htmlStr = '<ol data-role="listview">';
      for (var i = 0, length = app.seasonTitles.length ; i < length ; i++)
      {
        htmlStr += '<li><a href="' + (i+1) + '">' + season[i] + '</a></li>';
      }
      htmlStr += "</ol>";
      $('#seasonLevelContent').hide().html(htmlStr).trigger('create').slideDown('slow');
    }
  }

  function episodeLevelScript(){
    $('#ep-title').text(app.seasonTitles[app.getEpAsInt()-1]);
    if (app.getEpAsInt() === 1) { $('#navPrev').addClass('ui-disabled'); }
    if (app.getEpAsInt() === (app.seasonTitles.length)) { $('#navNext').addClass('ui-disabled'); }
  }

  function movieLevelScript(){
    $('#film-title').text(app.movies[app.getSeason()-1]);
    if (app.getSeason() === '1') { $('#navPrev').addClass('ui-disabled'); }
    if (app.getSeason() === ((app.movies.length).toString())) { $('#navNext').addClass('ui-disabled'); }
  }

  function ajaxCall(type){
    $.ajax({
    type: "POST",
    url: "http://" + app.getHost() + "/MemoryGamma/system/ajaxHandler.php",
    data: ajaxData[type](),
    success: ajaxData.successCallback,
    dataType: 'text' });
  }

  function nextEp(){
    var nextEpStr; 
    if (app.getShow() != 'Movies')
    {
      nextEpStr = 'http://'+app.getHost()+'/MemoryGamma/Star-Trek/'+app.getShow()+'/'+app.getSeason()+'/'+(app.getEpAsInt()+1)+'/';
    }
    if (app.getShow() === 'Movies')
    {
      nextEpStr = 'http://' + app.getHost() + '/MemoryGamma/Star-Trek/Movies/'+ (+app.getSeason() + 1)+'/';
    }
    $.mobile.changePage(nextEpStr, {transition:"slideup"}); 
  }

  function prevEp(){
    var prevEpStr;
    if (app.getShow() != 'Movies')
    {
      prevEpStr = 'http://'+app.getHost()+'/MemoryGamma/Star-Trek/'+app.getShow()+'/'+app.getSeason()+'/'+(app.getEpAsInt()-1)+'/';
    }
    if (app.getShow() === 'Movies')
    {
      prevEpStr = 'http://' + app.getHost() + '/MemoryGamma/Star-Trek/Movies/'+ (+app.getSeason() - 1)+'/';
    }
    $.mobile.changePage(prevEpStr, {transition:"slideup"}); 
  }

  function registerScript(){
    $('#btnSubmit').button('disable');
    function isSnValid(){
      return ($('#username').val().match(/^[\w-_]{6,25}$/) !== null);
    }
    function isPassValid(){
      return ($('#pwd').val().match(/^[\w-_!@#$%^&*()+`~\\\/]{6,30}$/) !== null);
    }
    $('#pwd2').keyup(function(){
      if ( (isSnValid() === true) && (isPassValid() === true) && ($('#pwd').val() === $(this).val()) )
      {
        $('#btnSubmit').button('enable');
      }
      else if ($('#pwd').val() !== $(this).val())
      {
        $('#btnSubmit').button('disable');
      }
    });
  }