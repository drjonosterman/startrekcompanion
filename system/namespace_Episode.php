<?php namespace episode;

	function getDbConnection(){
		require_once "dbConnString.php";
		return getDbConn();
	}

	function setLastPosition($userId, $lastPosition){
		$qry = getDbConnection()->prepare('UPDATE tb_user SET userBookmark = :bm WHERE userId = :uId');
		$qry->execute(array(':bm'=>$lastPosition, 'uId'=>$userId));
	}

	function favoriteEpisode($userId, $epId){
		$currentFaves = getFavorites($userId);
		$e = explode('-', $epId);
		if (!in_array($e[2], $currentFaves[$e[0]][$e[1]]))
		{
			array_push($currentFaves[$e[0]][$e[1]], $e[2]);
			setFavorites($userId, json_encode($currentFaves));
		}
		else { return; }
	}

	function getFavorites($userId){
		$qry = getDbConnection()->prepare('SELECT userFavorites FROM tb_user WHERE userId = ?');
		$qry->execute(array($userId));
		$qry->setFetchMode(\PDO::FETCH_NUM);
		$qryResult = $qry->fetch();
		return json_decode($qryResult[0], true);
	}

	function setFavorites($userId, $faves){
		$qry = getDbConnection()->prepare('UPDATE tb_user SET userFavorites = :faves WHERE userId = :uId');
		$qry->execute(array(':faves'=> $faves, ':uId'=>$userId));
	}

	function rateEpisode($userId, $epId, $rating){
		$tempRatings = getRatings($userId);
		$e = explode('-', $epId);
		if (!array_key_exists($e[2], $tempRatings[$e[0]][$e[1]]))
		{
			array_push($tempRatings[$e[0]][$e[1]], $e[2]);
		}
		$tempRatings[$e[0]][$e[1]][$e[2]] = $rating;
		setRatings($userId, json_encode($tempRatings));
	}

	function getRatings($userId){
		$qry = getDbConnection()->prepare('SELECT userRatings FROM tb_user WHERE userId = ?');
		$qry->execute(array($userId));
		$qry->setFetchMode(\PDO::FETCH_NUM);
		$qryResult = $qry->fetch();
		return json_decode($qryResult[0], true);
	}

	function setRatings($userId, $ratings){
		$qry = getDbConnection()->prepare('UPDATE tb_user SET userRatings = :rates WHERE userId = :uId');
		$qry->execute(array(':rates'=> $ratings, ':uId'=>$userId));
	}

	function noteEpisode($userId, $epId, $note){
		$tempNotes = getRatings($userId);
		$e = explode('-', $epId);
		if (!array_key_exists($e[2], $tempNotes[$e[0]][$e[1]]))
		{
			array_push($tempNotes[$e[0]][$e[1]], $e[2]);
		}
		$tempNotes[$e[0]][$e[1]][$e[2]] = $note;
		setNotes($userId, json_encode($tempNotes));
	}

	function getNotes($userId){
		$qry = getDbConnection()->prepare('SELECT userNotes FROM tb_user WHERE userId = ?');
		$qry->execute(array($userId));
		$qry->setFetchMode(\PDO::FETCH_NUM);
		$qryResult = $qry->fetch();
		return json_decode($qryResult[0], true);
	}
	
	function setNotes($userId, $notes){
		$qry = getDbConnection()->prepare('UPDATE tb_user SET userNotes = :notes WHERE userId = :uId');
		$qry->execute(array(':notes'=> $notes, ':uId'=>$userId));
	}
?>