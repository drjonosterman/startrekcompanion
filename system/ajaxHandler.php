<?php 
session_start();
header('Access-Control-Allow-Origin: http://www.ncc1705.com');  

if (!isset($_POST['type']))
{ 
  echo "Invalid/malformed request"; 
}

else
{
  require_once "../system/namespace_Episode.php";
  $uId = isset($_SESSION['userId']) ? $_SESSION['userId'] : NULL;
  
  if ($_POST['type'] === 'returnSeason')
  {
    require_once "../system/StarTrekEpisodeData.php";
    $stObj = new StarTrek();
    echo $stObj->requestSeason($_POST['showId'], $_POST['seasonId']); 
  }
  
  if ($_POST['type'] === 'getRating')
  {
    episode\rateEpisode($uId, $_POST['epId'], $_POST['rating']);
  }
  
  if ($_POST['type'] === 'setBookmark')
  {
    episode\setLastPosition($uId, $_POST['epId']);
  }
  
  if ($_POST['type'] === 'saveFavorite')
  {
    episode\favoriteEpisode($uId, $_POST['epId']);
  }
  
  if ($_POST['type'] === 'saveNote')
  {
    episode\noteEpisode($uId, $_POST['epId'], $_POST['note']);
  }
}
?>