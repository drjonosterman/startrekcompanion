<?php
namespace System;

	function encryptPass($passParam, $workFactor = 7){
		return crypt($passParam, '$2y$07$IWillGetABetterRandomS$');
	}

	function getSalt(){
		return substr(md5(mcrypt_create_iv(22, MCRYPT_RAND)), 0, 22);
	}

	function doesPassMatch($userPassword, $dbPassword){
		return crypt($userPassword, $dbPassword) === $dbPassword;}

	function isPassValid($passParam){ //Length between 6 and 30 characters, no spaces
		return strlen($passParam) < 6 || strlen($passParam) > 30 ? false : true;
	}

	function isSnUnique($snParam){
		$qry = getDbConnection()->prepare('SELECT userName FROM tb_user WHERE userName = ?');
		$qry->execute(array($snParam));
		return $qry->rowCount() < 1;
	}

	function isSnValid($snParam){
		if (strlen($snParam) < 6 || strlen($snParam > 25)) { return false;}
		if (!ctype_alnum(str_replace(array('-', '_'), '', $snParam))) { return false;} 
		else { return true; }
	}

	function isEmailUnique($emailParam){
		$qry = getDbConnection()->prepare('SELECT userEmail FROM tb_user WHERE userEmail = ?');
		$qry->execute(array($emailParam));
		return $qry->rowCount() < 1;
	}

	function fetchDbPassword($snParam){
		$qry = getDbConnection()->prepare('SELECT userPassword FROM tb_user WHERE userName = ?');
		$qry->execute(array($snParam));
		$qry->setFetchMode(\PDO::FETCH_NUM); 
		$qryResult = $qry->fetch();
		return $qryResult[0];
	}

	function fetchUserId($snParam){
		$qry = getDbConnection()->prepare('SELECT userId FROM tb_user WHERE userName = ?');
		$qry->execute(array($snParam));
		$qry->setFetchMode(\PDO::FETCH_NUM); 
		$qryResult = $qry->fetch();
		return $qryResult[0];
	}

	function getDbConnection(){
		require_once "dbConnString.php";
		return getDbConn();
	}

	function validateUser($snField, $pwdField){
		$valMessage = '';
		$valMessage .= (isSnValid($snField)) ? '' : 'Username contains invalid characters. ';
		$valMessage .= (isSnUnique($snField)) ? '' : 'Username is already taken. ';
		$valMessage .= (isPassValid($pwdField)) ? '' : 'Password is invalid.';
		if ($valMessage === '' ) { return true; }
		else { return $valMessage; }
	}

	function insertUser($sn, $pwd){
		$defaults = json_encode(array(
		'TOS'=>array('1'=>array(), '2'=>array(), '3'=>array()),
		'TAS'=>array('1'=>array(), '2'=>array()),
		'TNG'=>array('1'=>array(), '2'=>array(), '3'=>array(), '4'=>array(), '5'=>array(), '6'=>array(), '7'=>array()),
		'DS9'=>array('1'=>array(), '2'=>array(), '3'=>array(), '4'=>array(), '5'=>array(), '6'=>array(), '7'=>array()),
		'VOY'=>array('1'=>array(), '2'=>array(), '3'=>array(), '4'=>array(), '5'=>array(), '6'=>array(), '7'=>array()),
		'ENT'=>array('1'=>array(), '2'=>array(), '3'=>array(), '4'=>array(), '5'=>array()),
		'MOV'=>array()));

		$qry = getDbConnection()->prepare("INSERT INTO tb_user (userName, userPassword, userRatings, userNotes, userFavorites) VALUES (:sn, :pwd, :rates, :notes, :faves)");
		$qry->execute(array(':sn' => $sn, ':pwd' => encryptPass($pwd), ':rates'=>$defaults, ':notes'=>$defaults, ':faves'=>$defaults));
	}

?>
