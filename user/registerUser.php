<?php require_once "../system/sessionHandler.php"; ?>
<!DOCTYPE html>
<html>
<head>
    <title>Star Trek Companion</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <div data-role="page">
        <?php echo '<script type="text/javascript" src="http://'.$host.'/MemoryGamma/lib/jsLoadCheck.js"></script>'; ?>
        <div data-role="header" data-theme="b">
        <?php require_once "../template/header.php"; ?>
        </div>
        <div data-role="content" data-theme="b">
            <?php
            require_once "../system/namespace_System.php";
            if (isset($_POST['sn'], $_POST['pass']))
            {
              $regResult = System\validateUser($_POST['sn'], $_POST['pass']);
              if ($regResult === true) 
              { 
                System\insertUser($_POST['sn'], $_POST['pass']);
                echo 'Welcome, you have successfully registered. <a href="logIn.php">You may now log in.</a>';
              }
              else { echo $regResult;}
            }
            else {echo 'YOU SHALL NOT PASS!';}
            ?>
        </div>
        <div data-role="footer" id="test" data-theme="b">
            <?php include_once "../template/footer.php"; ?>
        </div>
    </div>
</body>
</html>