<?php require_once "../system/sessionHandler.php"; ?>
<!DOCTYPE html>
<html>
<head>
    <title>Star Trek Compation</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <div data-role="page">
        <?php echo '<script type="text/javascript" src="http://'.$host.'/MemoryGamma/lib/jsLoadCheck.js"></script>'; ?>
        <div data-role="header" data-theme="b">
        <?php require_once "../template/header.php"; ?>
        </div>
        <div data-role="content" data-theme="b">
            <?php require_once "../system/namespace_System.php";
            if (isset($_POST['sn'], $_POST['pass']))
            {
            	if (System\isSnValid($_POST['sn']) && System\isPassValid($_POST['pass']))
                {
                    if (System\doesPassMatch($_POST['pass'], System\fetchDbPassword($_POST['sn'])) === true)
                    {
                        $_SESSION['userId'] = System\fetchUserId($_POST['sn']);
                        $isLoggedIn = true;
                        echo 'Password matches. <a href=\'../Star-Trek/\'>You may now go to the catalog</a>';
                    }
                    else 
                    {
                        echo 'Nope';
                    }
                }                
                else
                {
                    echo 'Your username and/or password is not in the correct format';
                }
            }
            else { echo 'You shall not pass!';} ?>
        </div>
        <div data-role="footer" id="test" data-theme="b">
            <?php include_once "../template/footer.php"; ?>
        </div>
    </div>
</body>
</html>