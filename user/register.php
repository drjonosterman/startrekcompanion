<?php require_once "../system/sessionHandler.php"; ?>
<!DOCTYPE html>
<html>
<head>
    <title>Star Trek Compation</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <div data-role="page" id="page-Register">
        <?php echo '<script type="text/javascript" src="http://'.$host.'/MemoryGamma/lib/jsLoadCheck.js"></script>'; ?>
        <div data-role="header" data-theme="b">
        <?php require_once "../template/header.php"; ?>
        </div>
        <div data-role="content" data-theme="b">
            <form action="registerUser.php" name="registerForm" method="POST">

                <label for="username">Username (6-25 characters, letters, numbers, underscores, and dashes permitted)</label>
                <input type="text" name="sn" id="username" value="" />

                <label for="password">Password (6 and 30 characters, and no spaces)</label>
                <input type="password" name="pass" id="pwd" value="" />

                <label for="password">Confirm Password</label>
                <input type="password" name="pass2" id="pwd2" value="" />

                <input type="submit" value="Submit" id="btnSubmit" data-transition="slide" />
            </form>
        </div>
        <div data-role="footer" id="test" data-theme="b">
            <?php include_once "../template/footer.php"; ?>
        </div>
    </div>
</body>
</html>